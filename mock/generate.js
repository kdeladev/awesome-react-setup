const mocker = require('mocker-data-generator').default;
const util = require('util');
const fs = require('fs');
const path = require('path');

const json = {
  categories: require('./json/categories'),
  products: require('./json/products'),
  descriptions: require('./json/descriptions'),
  images: require('./json/images'),
  reviews: require('./json/reviews'),
  reviewsAuthors: require('./json/reviewsAuthors'),
}

const reviews = {
  id: {
    faker: 'random.uuid'
  },
  text: {
    values: json.reviews
  },
  author: {
    values: json.reviewsAuthors
  }
}

const categories = {
  id: {
    faker: 'random.uuid'
  },
  name: {
    values: json.categories
  },
  parentCategory: {
    values: ['man', 'woman', 'kids']
  }
}

const products = {
  id: {
    faker: 'random.uuid'
  },
  name: {
    values: json.products
  },
  description: {
    values: json.descriptions
  },
  image: {
    values: json.images
  },
  price: {
    chance: 'integer({"min": 30, "max": 500})'
  },
  categoryId: {
    hasOne: 'categories',
    get: 'id'
  },
  reviews: {
    hasMany: 'reviews',
    min: 1,
    max: 3
  }
}

const unique = (arr) => [...new Set(arr)];

mocker()
  .schema('reviews', reviews, { uniqueField: 'text'})
  .schema('categories', categories, { uniqueField: 'name'})
  .schema('products', products, { uniqueField: 'name' })
  .build()
  .then(
    data => {
      data.categories = data.categories.map(category => {
        category.products = unique(category.products);
        return category;
      })

      data.products = data.products.map(product => {
        product.reviews = unique(product.reviews);
        return product;
      })

      fs.writeFile(path.join(__dirname, 'db.json'), JSON.stringify(data, null, 2), 'utf8', () => {
        console.log('db generated');
      });
    },
    err => console.log(err)
);
