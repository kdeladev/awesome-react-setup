const rewireReactHotLoader = require('react-app-rewire-hot-loader');
const rewireLess = require('react-app-rewire-less');
const { injectBabelPlugin } = require('react-app-rewired');

module.exports = function override(config, env) {
  config = rewireReactHotLoader(config, env);
  config = rewireLess(config, env);
  config = injectBabelPlugin('transform-decorators-legacy', config);

  return config;
}
