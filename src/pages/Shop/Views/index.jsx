export { default as CategoryView } from './Category';
export { default as MainView } from './Main';
export { default as CheckoutView } from './Checkout';
