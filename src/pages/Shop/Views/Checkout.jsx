// jslint disable
import React, { Component } from 'react';
import { routeNode, withRoute } from 'react-router5';
import { connect } from 'react-redux';
import { Flex, Box} from 'grid-styled';
import ui from 'redux-ui';

import { getCartItems, getCartCount, getCartSummary } from './../../../redux/cart/selectors';
import { removeItemFromCart } from './../../../redux/cart/actions';
import CartTableContainer from './../../../containers/CartTableContainer';
import CheckoutForm from './../Forms/CheckoutForm';

@routeNode('shop.checkout')
@connect(
  state => ({
    items: getCartItems(state),
    summary: getCartSummary(state)
  }),
  dispatch => ({
    handleCheckoutForm: (formValues) => console.log(formValues)
  })
)
class Checkout extends Component {
  render() {
    return (
      <div>
        <CartTableContainer />
        <Flex justify="flex-end">
          <div>Summary: ${this.props.summary.totalPrice}</div>
        </Flex>
        <Flex style={{ marginTop: 30 }}>
          <Box w={1/2}>
            <h3>Checkout form</h3>
            <CheckoutForm onSubmit={(formValues) => this.props.handleCheckoutForm(formValues)} />
          </Box>
        </Flex>
      </div>
    );
  }
}

export default Checkout;
