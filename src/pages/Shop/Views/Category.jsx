import React, { Component } from 'react';
import { routeNode, withRoute } from 'react-router5';
import { connect } from 'react-redux';
import get from 'lodash/get';

import { fetchProducts } from './../../../redux/products/actions';
import { addItemToCart, removeItemFromCart } from './../../../redux/cart/actions';

import { getProductsByCurrentCategoryId } from './../../../redux/products/selectors';

import ProductsGrid from './../../../components/ProductsGrid';

@withRoute
@connect(
  state => {
    const stateId = get(state, 'router.route.params.categoryId');

    return {
      categoryId: stateId,
      products: getProductsByCurrentCategoryId(state)
    }
  },
  dispatch => ({
    fetchProducts: (categoryId) => dispatch(fetchProducts(categoryId)),
    addItemToCart: (item) => dispatch(addItemToCart(item)),
    removeItemFromCart: (item) => dispatch(removeItemFromCart(item)),
  })
)
class Category extends Component {
  componentDidMount() {
    this.props.fetchProducts(this.props.categoryId);
  }
  componentWillReceiveProps(nextProps) {
    if(this.props.categoryId !== nextProps.categoryId) {
      this.props.fetchProducts(nextProps.categoryId);
    }
  }
  handleProductClick = (product) => {
    const { route, router } = this.props;

    router.navigate(route.name, Object.assign({}, route.params, {
      productModal: product.id
    }));
  }
  render() {
    return (
      <div>
        <ProductsGrid products={this.props.products} onProductClick={(product) => this.handleProductClick(product) } />
      </div>
    );
  }
}

export default Category;
