import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actions } from 'redux-router5';
import { routeNode } from 'react-router5';

import { getCurrentSectionCategories, getCurrentCategoryId } from '../../redux/categories/selectors';

import { Flex, Box } from 'grid-styled';
import { Layout, Menu, Icon } from 'antd';
import { StyledLayoutWrapper } from './Shop.styled';
import { MainView, CategoryView, CheckoutView } from './Views';
import NotFound from '../NotFound';
import Logo from './../../components/Logo';
import Cart from './../../containers/CartContainer';
import { withProductModalHOC } from './../../components/ProductModal';

const { Header, Content, Sider } = Layout;

const contentComponents = {
  'shop': MainView,
  'shop.category': CategoryView,
  'shop.checkout': CheckoutView,
}

@routeNode('shop')
@withProductModalHOC({
  param: 'productModal'
})
@connect(
  state => ({
    categories: getCurrentSectionCategories(state),
    selectedCategory: getCurrentCategoryId(state)
  }),
  dispatch => ({
    openCategory: (category) => {
      dispatch(actions.navigateTo('shop.category', { categoryId: category.id, section: category.parentCategory }))
    }
  })
)
class Shop extends Component {
  constructor(props) {
    super(props);

    this.state = this.getInitialState();
  }
  getInitialState = () => {
    return {
      collapsed: false
    }
  }
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }
  handleClick = (event) => {
    this.props.openCategory(event.item.props.category);
  }
  render() {
    const { route, router } = this.props;

    const ContentInner = contentComponents[route.name] || NotFound;

    return (
      <StyledLayoutWrapper>
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}
          style={{ overflow: 'auto', height: '100vh', position: 'fixed', left: 0 }}
        >
          <Logo collapsed={this.state.collapsed} onClick={() => router.navigate('home')} />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={[this.props.selectedCategory]} onClick={this.handleClick}>
            {this.props.categories.map(category => (
              <Menu.Item key={category.id} category={category}>
                <Icon type="user" />
                <span>{category.name}</span>
              </Menu.Item>
            ))}
          </Menu>
        </Sider>
        <Header style={{ background: '#fff', padding: 0, paddingLeft: 20, position: 'fixed', left: this.state.collapsed ? 80 : 200, right: 0 }}>
          <Flex justify="space-between">
            <Box>
              <Icon
                type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
                onClick={this.toggle}
                style={{ cursor: 'pointer' }}
              />
            </Box>
            <Box style={{ paddingRight: 25 }}>
              <Cart />
            </Box>
          </Flex>
        </Header>
        <Layout style={{ marginLeft: this.state.collapsed ? 80 : 200, paddingTop: 64 }}>
          <Layout>
            <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}>
              <ContentInner />
            </Content>
          </Layout>
        </Layout>
      </StyledLayoutWrapper>
    );
  }
}

export default Shop;
