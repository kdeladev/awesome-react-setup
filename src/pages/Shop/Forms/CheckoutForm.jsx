import React, { Component } from 'react';

import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
const FormItem = Form.Item;
const Option = Select.Option;

class CheckoutForm extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log(values);
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit}>
        <FormItem>
          {getFieldDecorator('fullName', {
            rules: [{ required: true, message: 'Please input your full name' }],
          })(
            <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="text" placeholder="Imię i nazwisko" />
          )}
        </FormItem>

        <FormItem>
          {getFieldDecorator('address')(
            <Input type="textarea" placeholder="Address" />
          )}
        </FormItem>

        <FormItem>
          {getFieldDecorator('city')(
            <Input type="textarea" placeholder="City" />
          )}
        </FormItem>

        <FormItem>
          {getFieldDecorator('postCode')(
            <Input type="textarea" placeholder="Post Code" />
          )}
        </FormItem>

        <FormItem label="Select payment type">
          {getFieldDecorator('paymentType', {
            rules: [
              { required: true, message: 'Please select payment type' },
            ],
          })(
            <Select placeholder="Please select payment type">
              <Option value="card">Card</Option>
              <Option value="transfer">Transfer</Option>
            </Select>
          )}
        </FormItem>

        <FormItem label="Select delivery type">
          {getFieldDecorator('deliveryType', {
            rules: [
              { required: true, message: 'Please select payment type' },
            ],
          })(
            <Select placeholder="Please select payment type">
              <Option value="store">Collect from store</Option>
              <Option value="shipment">Shipment</Option>
            </Select>
          )}
        </FormItem>

        <FormItem>
          <Button type="primary" htmlType="submit">Confirm</Button>
        </FormItem>
      </Form>
    )
  }
}

export default Form.create()(CheckoutForm);
