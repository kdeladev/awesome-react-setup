import styled from 'styled-components';

const StyledLayoutWrapper = styled.div`
  .trigger {
    font-size: 18px;
    line-height: 64px;
    padding: 0 24px;
    cursor: pointer;
    transition: color .3s;
  }

  .trigger:hover {
    color: #1890ff;
  }
`;

export {
  StyledLayoutWrapper
}
