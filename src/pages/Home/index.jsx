import React, { Component } from 'react';

import TileSectionSelector from '../../components/TileSectionSelector';

class Home extends Component {
  render() {
    return (
      <div>
        <TileSectionSelector />
      </div>
    );
  }
}

export default Home;
