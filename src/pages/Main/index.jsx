import React from 'react';
import { routeNode } from 'react-router5';
import { startsWithSegment } from 'router5-helpers';

import Home from '../Home';
import Shop from '../Shop';
import NotFound from '../NotFound';

const Main = ({ route }) => {
  const { name } = route;

  const testRoute = startsWithSegment(name);

  if(testRoute('home')) {
    return <Home />
  } else if (testRoute('shop')) {
    return <Shop />
  }

  return <NotFound />;
}

export default routeNode('')(Main);
