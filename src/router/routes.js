const routes = [
  {
    name: 'home',
    path: '/'
  },
  {
    name: 'shop',
    path: '/shop',
    children: [
      {
        name: 'category',
        path: '/:section/:categoryId'
      },
      {
        name: 'checkout',
        path: '/checkout'
      }
    ]
  }
];

export default routes;
