import { RSAA, isRSAA } from 'redux-api-middleware';

const endpointMiddleware = store => next => action => {
  if(isRSAA(action)) {
    const endpoint = action[RSAA].endpoint;
    action[RSAA].endpoint = `${process.env.REACT_APP_API_URL}${endpoint}`;
  }

  return next(action);
}

export default endpointMiddleware;
