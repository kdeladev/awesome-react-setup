import { connect } from 'react-redux';
import { actions } from 'redux-router5';

import { Cart } from '../components/Cart';

import { getCartItems, getCartCount } from '../redux/cart/selectors';
import { removeItemFromCart } from './../redux/cart/actions';

const mapStateToProps = state => {
  return {
    items: getCartItems(state),
    count: getCartCount(state)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onItemRemove: (item) => dispatch(removeItemFromCart(item)),
    onCheckoutClick: () => dispatch(actions.navigateTo('shop.checkout'))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);

