import { connect } from 'react-redux';
import { actions } from 'redux-router5';

import { CartTable } from '../components/Cart';

import { getCartItems, getCartCount } from '../redux/cart/selectors';
import { removeItemFromCart, updateQuantity } from './../redux/cart/actions';

const mapStateToProps = state => {
  return {
    items: getCartItems(state)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onQuantityChange: (item, quantity) => dispatch(updateQuantity(item.id, quantity)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CartTable);

