import React from 'react';
import ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

import './index.less';
import 'antd/dist/antd.less';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import router from './router';

const render = Component => {
  ReactDOM.render(
    <AppContainer>
      <Component />
    </AppContainer>,
    document.getElementById('root')
  );
}

registerServiceWorker();

router.start((err, state) => {
  render(App);
});

if(module.hot) {
  module.hot.accept('./App', () => {
    render(App);
  });
}
