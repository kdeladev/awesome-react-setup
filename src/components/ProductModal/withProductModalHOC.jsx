import React, { Component, PropTypes } from 'react';
import window from 'global/window';
import wrapDisplayName from 'recompose/wrapDisplayName';
import { createChangeEmitter } from 'change-emitter';
import get from 'lodash/get';
import Modal from 'react-modal';
import ProductModal from './ProductModal';

const Aux = props => props.children;

const withProductModalHOCWrapper = (config = {param: 'productModal'}) => {
  const withProductModalHOC = BaseComponent => {
    return class withProductModalHOC extends Component {
      static displayName = wrapDisplayName(BaseComponent, 'ProductModalHOC');
      static state = {
        isOpen: null
      }
      constructor(props) {
        super(props)

        this.router = this.props.router;
        this.listener = null;
      }
      componentWillMount() {
        this.onRouterChangeCallback(this.props.route);
        this.router.addListener(this.onRouterChangeCallback);
      }
      componentWillUnmount() {
        this.router.removeListener(this.onRouterChangeCallback);
      }
      onRouterChangeCallback = (state) => {
        const productId = get(state, `params.${config.param}`);

        this.setState({
          isOpen: !! productId,
          productId: productId
        });
      }
      onModalClose = () => {
        const { route, router } = this.props;

        const newParams = Object.assign({}, route.params);
        delete newParams[config.param];

        router.navigate(route.name, newParams);
      }
      render() {
        return (
          <Aux>
            {this.state.isOpen && <ProductModal productId={this.state.productId} onClose={() => this.onModalClose()} />}
            <BaseComponent {...this.props} />
          </Aux>
        )
      }
    }
  }

  return withProductModalHOC;
}

export default withProductModalHOCWrapper;
