import React, { Component } from 'react';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import get from 'lodash/get';
import ProductModalContent from './ProductModalContent';
import { addItemToCart } from './../../redux/cart/actions';
import { fetchReviews } from './../../redux/products/actions';

@connect(
  (state, ownProps) => ({
    product: get(state, `products.productsByIds.${ownProps.productId}`),
    reviews: get(state, `products.reviewsByProductIds.${ownProps.productId}`),
  }),
  (dispatch, ownProps) => ({
    addToCart: (product) => dispatch(addItemToCart(product)),
    fetchReviews: (product) => dispatch(fetchReviews(ownProps.productId))
  })
)
class ProductModal extends Component {
  static defaultProps = {
    onClose: () => {}
  }
  componentDidMount = () => {
    this.props.fetchReviews(this.props.product)
  }
  render() {
    return (
      <Modal isOpen={true} onRequestClose={() => this.props.onClose()}>
        {
          this.props.product &&
            <ProductModalContent onAddToCart={() => this.props.addToCart(this.props.product)} reviews={this.props.reviews} {...this.props.product} />
        }
      </Modal>
    )
  }
}

Modal.setAppElement('body');

export default ProductModal;
