import React from 'react';
import { Button } from 'antd';

const ProductModalContent = ({ name, description, image, price, onAddToCart, reviews }) => (
  <div style={{ textAlign: 'center' }}>
    <h3>{name}</h3>
    <img src={image} style={{maxHeight: 300, maxWidth: '100%'}} />
    <div>{description}</div>
    <hr />
    <div>
      <Button onClick={() => onAddToCart()}>Add to cart - ${price}</Button>
    </div>
    <hr />
    <div style={{ textAlign: 'left' }}>
      <h3>Product reviews:</h3>
      {
        reviews && reviews.map(review => (
          <div key={review.id} style={{marginBottom: 10}}><strong>{review.author}</strong>: {review.text}</div>
        ))
      }
    </div>
  </div>
)

ProductModalContent.defaultProps = {
  onAddToCart: () => {}
}

export default ProductModalContent;
