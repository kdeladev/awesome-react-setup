import React, { Component } from 'react';

import {
  ProductsGridContainer,
  Product
} from './ProductsGrid.styled';

class ProductsGrid extends Component {
  static defaultProps = {
    products: [],
    onProductClick: () => {}
  }
  render() {
    return (
      <ProductsGridContainer wrap>
        {this.props.products.map(product => (
          <Product key={product.id} width={[ 1/1, 1/2, 1/2, 1/3]} onClick={() => this.props.onProductClick(product)}>
            <img src={product.image} />
            <div>
              <div className="name">{product.name}</div>
              <div className="price">${product.price}</div>
            </div>
          </Product>
        ))}
      </ProductsGridContainer>
    );
  }
}

export default ProductsGrid;

// {this.props.products && this.props.products.map(product => (
//   <div key={product.id} style={{marginTop: '30px'}}>
//     {product.id}
//     <div>
//       <button onClick={() => this.props.addItemToCart(product)}>add to cart</button>
//       <button onClick={() => this.props.removeItemFromCart(product)}>remove from cart</button>
//     </div>
//   </div>
// ))}
