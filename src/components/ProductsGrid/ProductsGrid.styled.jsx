import styled from 'styled-components';
import { Flex, Box } from 'grid-styled';

const ProductsGridContainer = styled(Flex)`
`;

const Product = styled(Box)`
  padding: 20px 20px 0px 20px;
  cursor: pointer;

  img {
    width: 100%;
    margin-left: auto;
    margin-right: auto;
  }

  .name, .price {
    font-weight: 500;
  }
`;

export {
  ProductsGridContainer,
  Product
}
