import React, { Component } from 'react';
import { Badge, Icon, Dropdown, Button } from 'antd';
import styled from 'styled-components';
import { Flex, Box } from 'grid-styled';

const CartContentContainer = styled.div`
  right: 0px;
  width: 300px;
  height: 300px;
  background-color: #fff;
  border: 1px solid #e0e0e0;
  border-radius: 3px;
  box-shadow: 0px 2px 3px #b7b7b763;

  .title {
    font-size: 1.2rem;
    padding: 10px;
    border-bottom: 1px solid #cecece;
    margin-bottom: 10px;
  }

  .item {
    padding: 5px 10px 5px 10px;
  }

  .checkout {
    padding: 10px;
    border-top: 1px solid #cecece;
    margin-bottom: 10px;
  }

  .listWrapper {
    height: 185px;
    overflow-y: auto;
  }
`;


class Cart extends Component {
  static defaultProps = {
    onItemRemove: () => {},
    onCheckoutClick: () => {}
  }
  handleRemoveItem = (event, item) => {
    this.props.onItemRemove(item);
    event.stopPropagation();
  }
  render() {
    const CartContent = (
      <CartContentContainer>
        <div className="title">Cart</div>
        <div className="listWrapper">
          { this.props.items.map(item => (
            <Flex className="item" key={item.id}>
              <Box flex="1 1 auto">
                {item.name}
              </Box>
              <Box>
                <Icon type="close" style={{ fontSize: 18, cursor: 'pointer' }} onClick={(event) => this.handleRemoveItem(event, item) } />
              </Box>
            </Flex>
          ))}
        </div>
        <div className="checkout">
          <Button onClick={() => this.props.onCheckoutClick() }>Checkout</Button>
        </div>
      </CartContentContainer>
    )

    return (
      <Dropdown overlay={CartContent} trigger={['click']}>
        <Badge count={this.props.count}>
          <Icon type="shopping-cart" style={{ fontSize: 18, cursor: 'pointer' }} />
        </Badge>
      </Dropdown>
    );
  }
}

export default Cart;
