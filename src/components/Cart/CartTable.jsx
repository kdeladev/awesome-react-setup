import React, { Component } from 'react';
import { Table, Icon } from 'antd';
import styled from 'styled-components';
import { Flex, Box } from 'grid-styled';

const StyledIcon = styled(Icon)`
  cursor: pointer;
  user-select: none;
`;

class CartTable extends Component {
  static defaultProps = {
    onQuantityChange: () => {}
  }
  getColumns = () => {
    return [{
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    }, {
      title: 'Quantity',
      dataIndex: 'quantity',
      key: 'quantity',
      render: (text, record) => (
        <div>
          <StyledIcon type="minus-circle-o" onClick={() => this.handleDecreaseQuantity(record) } />
          <span style={{ marginLeft: 20, marginRight: 20 }}>{record.quantity}</span>
          <StyledIcon type="plus-circle-o" onClick={() => this.handleIncreaseQuantity(record) }/>
        </div>
      )
    }, {
      title: 'Price',
      key: 'price',
      render: item => `$${item.price}`
    }, {
      title: 'Summary',
      key: 'summary',
      render: item => `$${item.price * item.quantity}`
    }];
  }
  handleQuantityChange = (item, quantity) => {
    this.props.onQuantityChange(item, quantity);
  }
  handleDecreaseQuantity = (item) => {
    const newQuantity = item.quantity - 1;
    this.handleQuantityChange(item, newQuantity);
  }
  handleIncreaseQuantity = (item) => {
    const newQuantity = item.quantity + 1;
    this.handleQuantityChange(item, newQuantity);
  }
  render() {
    return (
      <Table rowKey={"id"} columns={this.getColumns()} dataSource={this.props.items} pagination={false} />
    );
  }
}

export default CartTable;
