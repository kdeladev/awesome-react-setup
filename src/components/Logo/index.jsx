import React from 'react';
import styled from 'styled-components';

const Logo = styled.div`
  color: #fff;
  height: 64px;
  text-align: center;
  line-height: 64px;
  cursor: pointer;

  font-size: ${props => props.collapsed ? '0.5rem' : '1rem' };

  span {
    font-size: ${props => props.collapsed ? '0.5rem' : '0.7rem' };
  }
`;

export default (props) => (
  <Logo {...props}>
    eCommerce.<span>io</span>
  </Logo>
);
