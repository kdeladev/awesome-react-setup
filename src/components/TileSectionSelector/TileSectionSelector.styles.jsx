import { Flex, Box } from 'grid-styled';
import styled from 'styled-components';

const TileSelectorContainer = styled(Flex)`
  height: 300px;
  align-items: stretch;
`;

const Tile = styled(Box)`
  flex: 1 0 auto;
  transition: transform .2s ease-in-out;
  cursor: pointer;
  min-height: 250px;
`;

const TileLabel = styled(Flex)`
  justify-content: center;
  align-items: center;
  height: 100%;
  font-size: 1.7rem;
  background-color: #ff000023;
  color: white;

  background-image: url(${props => props.bg});
  background-size: cover;
  text-shadow: 0px 0px 15px #000;
  user-select: none;
`;

export {
  TileSelectorContainer,
  Tile,
  TileLabel
}
