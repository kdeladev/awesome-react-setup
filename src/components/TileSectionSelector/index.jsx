import React, { Component } from 'react';
import { withRoute } from 'react-router5';

import {
  TileSelectorContainer,
  Tile,
  TileLabel
} from './TileSectionSelector.styles';

@withRoute
class TileSectionSelector extends Component {
  render() {
    const { router } = this.props;

    return (
      <TileSelectorContainer align="center" justify="center" wrap>
        <Tile w={[ 1, 1/3 ]} onClick={() => router.navigate('shop', { section: 'man'})} key="1">
          <TileLabel bg="/img/carousel-2.jpeg">Man</TileLabel>
        </Tile>
        <Tile w={[ 1, 1/3 ]} onClick={() => router.navigate('shop', { section: 'woman'})} key="2">
          <TileLabel bg="/img/carousel-1.jpeg">Woman</TileLabel>
        </Tile>
        <Tile w={[ 1, 1/3 ]} onClick={() => router.navigate('shop', { section: 'kids'})} key="3">
          <TileLabel bg="/img/carousel-3.jpeg">Kids</TileLabel>
        </Tile>
      </TileSelectorContainer>
    );
  }
}

export default TileSectionSelector;
