import { RSAA } from 'redux-api-middleware';

import {
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE,
  FETCH_REVIEWS_REQUEST,
  FETCH_REVIEWS_SUCCESS,
  FETCH_REVIEWS_FAILURE
} from './types';

export const fetchProducts = (categoryId) => {
  return {
    [RSAA]: {
      endpoint: `/categories/${categoryId}/products`,
      types: [{
        type: FETCH_PRODUCTS_REQUEST,
        meta: { categoryId }
      },
      {
        type: FETCH_PRODUCTS_SUCCESS,
        meta: { categoryId }
      },
      {
        type: FETCH_PRODUCTS_FAILURE,
        meta: { categoryId }
      }],
      method: 'GET'
    }
  }
};

export const fetchReviews = (productId) => {
  return {
    [RSAA]: {
      endpoint: `/products/${productId}/reviews`,
      types: [{
        type: FETCH_REVIEWS_REQUEST,
        meta: { productId }
      },
      {
        type: FETCH_REVIEWS_SUCCESS,
        meta: { productId }
      },
      {
        type: FETCH_REVIEWS_FAILURE,
        meta: { productId }
      }],
      method: 'GET'
    }
  }
};
