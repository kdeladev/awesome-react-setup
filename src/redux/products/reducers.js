import update from 'immutability-helper';
import keyBy from 'lodash/keyBy';

import {
  FETCH_PRODUCTS_REQUEST,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_REVIEWS_SUCCESS
} from './types';

const initialState = {
  productsByIds: {},
  reviewsByProductIds: {}
};

const productsReducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_PRODUCTS_SUCCESS:
      return update(state, {
        productsByIds: {
          $merge: keyBy(action.payload, 'id')
        }
      })
    case FETCH_REVIEWS_SUCCESS:
      return update(state, {
        reviewsByProductIds: {
          [action.meta.productId]: {
            $set: action.payload
          }
        }
      });
    default:
      return state;
  }
}

export default productsReducer;
