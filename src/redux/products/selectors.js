import { createSelector } from 'reselect';
import get from 'lodash/get';
import pickBy from 'lodash/pickBy';
import values from 'lodash/values';

const productsSelector = state => get(state, 'products.productsByIds');
const categoryId = state => get(state, 'router.route.params.categoryId');

const getProductsByCurrentCategoryId = createSelector(
  productsSelector, categoryId,
  (products, categoryId) => {
    return values(pickBy(products, {
      categoryId: categoryId
    }));
  }
)

export {
  getProductsByCurrentCategoryId
}
