import { RSAA } from 'redux-api-middleware';

import {
  FETCH_CATEGORIES_REQUEST,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_FAILURE
} from './types';

export const fetchCategories = () => {
  return {
    [RSAA]: {
      endpoint: `/categories`,
      types: [FETCH_CATEGORIES_REQUEST, FETCH_CATEGORIES_SUCCESS, FETCH_CATEGORIES_FAILURE],
      method: 'GET'
    }
  }
};
