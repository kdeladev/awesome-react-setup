import { createSelector } from 'reselect';
import get from 'lodash/get';

const categoriesSelector = state => get(state, 'categories.list');
const sectionSelector = state => get(state, 'router.route.params.section');
const selectedCategorySelector = state => get(state, 'router.route.params.categoryId');

const getCurrentSectionCategories = createSelector(
  categoriesSelector, sectionSelector,
  (categories, section) => categories.filter(category => category.parentCategory === section)
)

const getCurrentCategoryId = createSelector(
  selectedCategorySelector,
  categoryId => categoryId
)

export {
  getCurrentSectionCategories,
  getCurrentCategoryId
}
