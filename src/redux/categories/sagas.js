import { all, put} from 'redux-saga/effects';
import { fetchCategories } from './actions';

function* fetchCategoriesOnInit() {
  yield put(fetchCategories());
}

export default function* categoriesSagas() {
  yield all([
    fetchCategoriesOnInit()
  ]);
}
