import {
  FETCH_CATEGORIES_SUCCESS
} from './types';

const initialState = {
  list: []
}

const categoriesReducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_CATEGORIES_SUCCESS:
      return Object.assign({}, state, {
        list: action.payload
      });
    default:
      return state;
  }
}

export default categoriesReducer;
