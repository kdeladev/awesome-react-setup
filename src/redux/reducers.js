import app from './app/reducers';
import categories from './categories/reducers';
import products from './products/reducers';
import cart from './cart/reducers';

export default { app, categories, products, cart }
