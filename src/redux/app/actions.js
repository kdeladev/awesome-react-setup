import * as types from './types';

const actions = {
  setConfig: (config) => ({
    type: types.APP_GET_CONFIG,
    payload: {
      ...config
    }
  })
}
