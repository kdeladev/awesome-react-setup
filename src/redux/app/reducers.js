import * as types from './types';

const setConfig = (state, action) => {
  let config = Object.assign({}, state.config, action.payload);

  return { ...state, config }
}

const initialState = {
  config: {
  }
}

const appReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.APP_SET_CONFIG:
      return setConfig(state, action)
    default:
      return state;
  }
}

export default appReducer;
