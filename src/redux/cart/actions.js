import { RSAA } from 'redux-api-middleware';

import {
  FETCH_CART_REQUEST,
  FETCH_CART_SUCCESS,
  FETCH_CART_FAILURE,
  CART_ADD_ITEM,
  CART_REMOVE_ITEM,
  UPDATE_QUANTITY
} from './types';

export const fetchCart = () => {
  return {
    [RSAA]: {
      endpoint: `/cart`,
      types: [FETCH_CART_REQUEST, FETCH_CART_SUCCESS, FETCH_CART_FAILURE],
      method: 'GET'
    }
  }
}

export const addItemToCart = (item) => {
  return {
    type: CART_ADD_ITEM,
    payload: { item }
  }
}

export const removeItemFromCart = (item) => {
  return {
    type: CART_REMOVE_ITEM,
    payload: { item }
  }
}

export const updateQuantity = (itemId, quantity) => {
  return {
    type: UPDATE_QUANTITY,
    payload: { itemId, quantity }
  }
}
