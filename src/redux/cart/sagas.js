import { all, put, takeEvery } from 'redux-saga/effects';

import { removeItemFromCart } from './actions';

import {
  UPDATE_QUANTITY
} from './types';

function* validateQuantity(action) {
  if(action.payload.quantity <= 0) {
    yield put(removeItemFromCart({
      id: action.payload.itemId
    }));
  }
}

function* quantitySaga() {
  yield takeEvery(UPDATE_QUANTITY, validateQuantity);
}

export default function* cartSagas() {
  yield all([
    quantitySaga()
  ]);
}
