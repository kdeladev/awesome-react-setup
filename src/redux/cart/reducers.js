import update from 'immutability-helper';

import {
  FETCH_CART_SUCCESS,
  CART_ADD_ITEM,
  CART_REMOVE_ITEM,
  UPDATE_QUANTITY
} from './types';

const initialState = {
  items: {},
  quantity: {}
}

const cartReducer = (state = initialState, action) => {
  switch(action.type) {
    case FETCH_CART_SUCCESS:
      return Object.assign({}, state, {
        items: action.payload
      });
    case CART_ADD_ITEM:
      const id = action.payload.item.id;
      const quantity = state.quantity[id];

      return update(state, {
        items: {
          $merge: {
            [id]: action.payload.item
          }
        },
        quantity: {
          $merge: {
            [id]: quantity ? quantity + 1 : 1
          }
        }
      })
    case CART_REMOVE_ITEM:
      return update(state, {
        items: {
          $unset: [action.payload.item.id]
        },
        quantity: {
          $unset: [action.payload.item.id]
        }
      })
    case UPDATE_QUANTITY:
      return update(state, {
        quantity: {
          $merge: {
            [action.payload.itemId]: action.payload.quantity
          }
        }
      });
    default:
      return state;
  }
}

export default cartReducer;
