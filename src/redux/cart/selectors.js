import { createSelector } from 'reselect';
import get from 'lodash/get';
import reduce from 'lodash/reduce';

const itemsSelector = state => get(state, 'cart.items');
const quantitySelector = state => get(state, 'cart.quantity');

const getCartItems = createSelector(
  itemsSelector, quantitySelector,
  (items, quantity) => {
    return Object.values(items).map(item => {
      item.quantity = quantity[item.id];

      return item;
    });
  }
)

const getCartCount = createSelector(
  itemsSelector,
  (items) => Object.values(items).length
)

const getCartSummary = createSelector(
  itemsSelector, quantitySelector,
  items => {
    const totalPrice = reduce(items, (acc, item) => {
      return acc + (item.quantity * item.price);
    }, 0);

    return {
      totalPrice: totalPrice
    }
  }
)

export {
  getCartItems,
  getCartCount,
  getCartSummary
}
