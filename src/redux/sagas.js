import { all } from 'redux-saga/effects';
import categoriesSagas from './categories/sagas';
import cartSagas from './cart/sagas';

export default function* rootSaga() {
  yield all([
    categoriesSagas(),
    cartSagas()
  ]);
}
