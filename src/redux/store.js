import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import { router5Middleware, router5Reducer } from 'redux-router5';

import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { apiMiddleware } from 'redux-api-middleware';
import { reduxPlugin } from 'redux-router5';
import { reducer as uiReducer } from 'redux-ui'

import persistState, {mergePersistedState} from 'redux-localstorage';
import adapter from 'redux-localstorage/lib/adapters/localStorage';
import filter from 'redux-localstorage-filter';

import endpointMiddleware from './../redux-utils/middlewares/endpoint';

import reducers from './reducers';
import rootSaga from './sagas';
import router from '../router';

const sagaMiddleware = createSagaMiddleware();
const routerMiddleware = router5Middleware(router);

const rootReducer = combineReducers({
  ...reducers,
  router: router5Reducer,
  ui: uiReducer
});

const rootMiddleware = applyMiddleware(
  routerMiddleware,
  thunk,
  sagaMiddleware,
  endpointMiddleware,
  apiMiddleware,
);

const reducer = compose(
  mergePersistedState()
)(rootReducer);

const storage = compose(
  filter('cart'),
)(adapter(window.localStorage));

const store = createStore(
  reducer,
  composeWithDevTools(
    compose(
      rootMiddleware,
      persistState(storage, 'store')
    )
  )
);

router.usePlugin(reduxPlugin(store.dispatch));

sagaMiddleware.run(rootSaga);

export { store };
