import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { RouterProvider } from 'react-router5';

import { store } from './redux/store';
import router from './router';

import Main from './pages/Main';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <RouterProvider router={router}>
          <Main />
        </RouterProvider>
      </Provider>
    );
  }
}

export default App;
